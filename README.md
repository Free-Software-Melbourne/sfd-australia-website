
# Software Freedom Day Australia

Website source code for Software Freedom Day Australia

## Artwork Credits
- Photos: Chris Samuel 2010 (CC BY-SA)
- FSM Designs: Michael Verrenkamp and Ben Minerds (CC0 1.0)
